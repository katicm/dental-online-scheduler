export const baseFirestore =
  'https://firestore.googleapis.com/v1/projects/dental-management-system-2020/databases/(default)/documents';

/*const config = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID,
};*/

export const mapTreatment = (item) => ({
  ...item.fields,
  id: item.name.split('/')[6],
});

const getFirestoreProp = (value) => {
  const props = {
    booleanValue: 1,
    integerValue: 1,
    stringValue: 1,
    mapValue: 1,
    timestampValue: 1,
  };
  return Object.keys(value).find((k) => props[k] === 1);
};

export const firestoreParser = (value) => {
  const prop = getFirestoreProp(value);
  if (prop === 'integerValue') {
    value = Number(value[prop]);
  } else if (prop === 'arrayValue') {
    value = ((value[prop] && value[prop].values) || []).map((v) => firestoreParser(v));
  } else if (prop === 'mapValue') {
    value = firestoreParser((value[prop] && value[prop].fields) || {});
  } else if (prop) {
    value = value[prop];
  } else if (typeof value === 'object') {
    Object.keys(value).forEach((k) => (value[k] = firestoreParser(value[k])));
  }
  return value;
};

//https://github.com/jdbence/firestore-parser

// prettier-ignore
export const mapToTeeth = (toothID, type, zone, procedure) => {
  return {
    "fields": {
        [toothID]: {
            "mapValue": {
                "fields": {
                    [type]: {
                        "mapValue": {
                            "fields": {
                                [zone]: {
                                    "stringValue": procedure
                                }
                            }
                        }
                    }
                }
            }
        }
    }
  }
}
//prettier-ignore
export const mapToTreatment=(newScheduleTime)=>{
  return {
    "fields": {
      "scheduledTime": {
        "timestampValue": newScheduleTime
      }
    }
  }
};
//prettier-ignore
export const treatmentQuery = (pacientID) => ({ 
  "structuredQuery": {
      "from": {
          "collectionId": "treatments"
      },
      "orderBy": {
          "field": {
              "fieldPath": "scheduledTime"
          },
          "direction": "ASCENDING"
      },
      "where": { 
          "compositeFilter": { 
              "filters": [
                  { "fieldFilter": { 
                      "field": { 
                          "fieldPath": "pacientID" 
                      }, 
                          "op": "EQUAL", 
                          "value": { 
                              "stringValue": "somerandompacinetid" 
                          } 
                      } 
                  }
              ], "op": "AND"
          } 
      }
  }
});
