import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import TreatmentsPage from './pages/TreatmentsPage';
import PatientTreatmentPage from './pages/PatientTreatmentPage';
import SchedulerPage from './pages/SchedulerPage';
import MainPage from './pages/MainPage';

const App = () => {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path="/" component={MainPage} />
        <Route exact path="/treatment/patient=:patientID" component={PatientTreatmentPage} />
        <Route exact path="/scheduler" component={SchedulerPage} />
        <Route exact path="/treatments" component={TreatmentsPage} />
      </Switch>
      <Footer />
    </div>
  );
};

export default App;
