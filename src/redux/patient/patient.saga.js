import { put } from 'redux-saga/effects';
import { setFetchedPatient_Success, setFetchedPatient_Fail } from './patient.actions';
import axios from 'axios';

export function* fetchPatientsSaga() {
  try {
    const response = yield axios.get(`${process.env.REACT_APP_BASE_LOCAL}/patient/extend`);
    if (response.status === 200) yield put(setFetchedPatient_Success(response.data));
  } catch (error) {
    put(setFetchedPatient_Fail(error));
  }
}
