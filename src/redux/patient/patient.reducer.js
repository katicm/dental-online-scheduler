import PatientActionTypes from './patient.types';

const INITIAL_STATE = {
  patients: [],
  isLoading: true,
  focused: {},
  error: null,
};

const patientReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PatientActionTypes.FETCH_PATIENT_START: {
      return { ...state, isLoading: true };
    }
    case PatientActionTypes.FETCH_PATIENT_SUCCESS: {
      return { ...state, patients: action.data, isLoading: false };
    }
    case PatientActionTypes.FETCH_PATIENT_ERROR: {
      return { ...state, error: action.error, isLoading: false };
    }
    case PatientActionTypes.FOCUS_PATIENT: {
      return {
        ...state,
        focused: state.patients.find((item) => item.patientID === action.patientID),
      };
    }
    default:
      return state;
  }
};
export default patientReducer;
