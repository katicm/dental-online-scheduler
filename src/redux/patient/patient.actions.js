import PatientActionTypes from './patient.types';

export const fetchPatients = () => ({
  type: PatientActionTypes.FETCH_PATIENT_START,
});
export const setFetchedPatient_Success = (data) => ({
  type: PatientActionTypes.FETCH_PATIENT_SUCCESS,
  data,
});
export const setFetchedPatient_Fail = (error) => ({
  type: PatientActionTypes.FETCH_PATIENT_ERROR,
  error,
});
export const focusPatient = (patientID) => ({
  type: PatientActionTypes.FOCUS_PATIENT,
  patientID,
});
