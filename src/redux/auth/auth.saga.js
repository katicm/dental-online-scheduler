import { put } from "redux-saga/effects";
import axios from "axios";
import { setAuth_Failure, setSignIn, setSignUp } from "./auth.action";
import moment from "moment";

export function* signInSaga(action) {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_LOCAL}/account/login/email=${action.data.email}&password=${action.data.password}`
      /*{
        headers: {
          Authorization:
            "Basic " +
            Buffer.from(
              `${action.data.email}:${action.data.password}`,
              "utf8"
            ).toString("base64"),
        },
      }*/
    );
    console.log(response.data);
    yield put(setSignIn(response.data));
  } catch (error) {
    yield put(setAuth_Failure(error.response.data));
  }
}

export function* signUpSaga(action) {
  try {
    const response = yield axios.post(
      `${process.env.REACT_APP_BASE_LOCAL}/sign-up`,
      {
        ...action.data,
        created: moment().format().split("+")[0],
        role: "Guest",
      }
    );
    if (response.status === 201) yield put(setSignUp(response.data));
  } catch (error) {
    yield put(setAuth_Failure(error));
  }
}
