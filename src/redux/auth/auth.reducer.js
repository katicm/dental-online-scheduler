import AuthActionTypes from './auth.types';

const INITAL_STATE = {
  isLoading: false,
  error: null,
  user: { doctorID: 1, doctorFullName: 'Ana Somerman', title: 'General dentist' },
};

const authReducer = (state = INITAL_STATE, action) => {
  switch (action.type) {
    case AuthActionTypes.SIGN_IN_START: {
      return { user: null, isLoading: true, error: null };
    }
    case AuthActionTypes.SET_SIGN_IN: {
      return {
        user: { account: action.payload },
        isLoading: false,
        error: null,
      };
    }
    case AuthActionTypes.SET_AUTH_FAILURE: {
      return {
        user: null,
        isLoading: false,
        error: action.error,
      };
    }
    case AuthActionTypes.SET_SIGN_OUT: {
      return {
        user: null,
        isLoading: false,
        error: null,
      };
    }
    case AuthActionTypes.SIGN_UP_START: {
      return {
        user: null,
        error: null,
        isLoading: true,
      };
    }
    case AuthActionTypes.SET_SIGN_UP: {
      return {
        error: null,
        data: action.payload,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};
export default authReducer;
