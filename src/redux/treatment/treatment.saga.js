import { put, all, fork } from 'redux-saga/effects';
import {
  setFetchedTreatments_Success,
  setFetchedTeeth_Success,
  setFetchedTreatments_Fail,
  toothUpdated,
  toothUpdated_Fail,
  addTreatment_Success,
  treatment_Fail,
  deleteTreatment_Success,
  checkTreatment_Success,
  updateScheduledTime_Success,
  updateScheduledTime_Fail,
  setAllFetchedTreatments_Fail,
  setAllFetchedTreatments_Success,
} from './treatment.actions';
import axios from 'axios';
import {
  firestoreParser,
  mapTreatment,
  baseFirestore,
  mapToTeeth,
} from '../../utils/firebase.utils';

export function* fetchTreatmentSaga(action) {
  yield all([fork(fetchTreatmentsSaga, action.patientID), fork(fetchTeethSaga, action.patientID)]);
}

function* fetchTreatmentsSaga(patientID) {
  try {
    const response = yield axios.get(
      `${process.env.REACT_APP_BASE_LOCAL}/treatment/patient=${patientID}`,
    );
    if (response.status === 200) yield put(setFetchedTreatments_Success(response.data));
  } catch (error) {
    put(setFetchedTreatments_Fail(error));
  }
}

function* fetchTeethSaga(patientID) {
  try {
    const response = yield axios.get(`${baseFirestore}/teeth/${patientID}`);
    if (response.status === 200)
      yield put(setFetchedTeeth_Success(firestoreParser(response.data.fields)));
  } catch (error) {
    put(setFetchedTreatments_Fail(error));
  }
}

export function* fetchAllTreatmentsSaga() {
  try {
    const response = yield axios.get(`${process.env.REACT_APP_BASE_LOCAL}/treatment/extend`);
    if (response.status === 200) yield put(setAllFetchedTreatments_Success(response.data));
  } catch (error) {
    put(setAllFetchedTreatments_Fail(error));
  }
}

export function* updateToothSaga(action) {
  try {
    const { toothID, type, zone, procedure } = action.data;

    const response = yield axios.patch(
      `${baseFirestore}/teeth/${action.patientID}?updateMask.fieldPaths=${toothID}.${type}.${zone}`,
      mapToTeeth(toothID, type, zone, procedure),
    );
    if (response.status === 200) yield put(toothUpdated());
  } catch (error) {
    put(toothUpdated_Fail(error));
  }
}

export function* checkTreatmentSaga(action) {
  try {
    const { toothID, zone, type, procedure, treatmentID } = action.data;
    const response = yield axios.patch(
      `${baseFirestore}/teeth/${action.patientID}?updateMask.fieldPaths=${toothID}.${type}.${zone}`,
      mapToTeeth(toothID, type, zone, procedure),
    );
    const response2 = yield axios.put(`${process.env.REACT_APP_BASE_LOCAL}/treatment`, action.data);
    if (response.status === 200 && response2.status === 200)
      yield all([
        put(checkTreatment_Success(firestoreParser(response.data.fields))),
        put(deleteTreatment_Success(treatmentID)),
      ]);
  } catch (error) {}
}

export function* addTreatmentSaga(action) {
  try {
    const response = yield axios.post(`${baseFirestore}/treatments`, action.data);
    firestoreParser(response.data);
    if (response.status === 200) yield put(addTreatment_Success(mapTreatment(response.data)));
  } catch (error) {
    put(treatment_Fail(error));
  }
}

export function* deleteTreatmentSaga(action) {
  try {
    const response = yield axios.delete(
      `${process.env.REACT_APP_BASE_LOCAL}/treatment/${action.treatmentID}`,
    );
    if (response.status === 200) yield put(deleteTreatment_Success(action.treatmentID));
  } catch (error) {
    put(treatment_Fail(error));
  }
}

export function* updateScheduledTimeSaga(action) {
  try {
    const response = yield axios.put(`${process.env.REACT_APP_BASE_LOCAL}/treatment`, action.data);
    if (response.status === 200) yield put(updateScheduledTime_Success());
  } catch (error) {
    put(updateScheduledTime_Fail(error));
  }
}
