import TreatmentActionTypes from './treatment.types';

const INITIAL_STATE = {
  procedure: 'none',
  mode: 'current',
  treatments: [],
  teeth: {},
  planned: [],
  loading: true,
  error: null,
};

const treatmentReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TreatmentActionTypes.CHANGE_PROCEDURE: {
      return { ...state, procedure: action.procedure };
    }
    case TreatmentActionTypes.CHANGE_MODE: {
      return { ...state, mode: action.mode };
    }
    case TreatmentActionTypes.FETCH_ALL_TREATMENTS: {
      return { ...state, treatments: [], loading: true };
    }
    case TreatmentActionTypes.SET_ALL_FETCHED_TREATMENTS_SUCCESS: {
      return { ...state, treatments: action.data, loading: false };
    }
    case TreatmentActionTypes.SET_ALL_FETCHED_TREATMETNS_FAIL: {
      return { ...state, loading: false, error: action.error };
    }
    case TreatmentActionTypes.START_FETCH_TREATMENT: {
      return { ...state, teeth: {}, loading: true };
    }
    case TreatmentActionTypes.SET_FETCHED_TREATMENTS_SUCCESS: {
      return {
        ...state,
        planned: action.data,
      };
    }
    case TreatmentActionTypes.SET_FETCHED_TEETH_SUCCESS: {
      return {
        ...state,
        teeth: action.data,
        loading: false,
      };
    }
    case TreatmentActionTypes.SET_FETCHED_TREATMENT_FAIL: {
      return { ...state, error: action.error, loading: false };
    }
    case TreatmentActionTypes.UPDATE_TOOTH: {
      return {
        ...state,
        teeth: {
          ...state.teeth,
          [action.data.toothID]: {
            ...state.teeth[action.data.toothID],
            [action.data.type]: {
              ...state.teeth[action.data.toothID][action.data.type],
              [action.data.zone]: action.data.procedure,
            },
          },
        },
      };
    }
    case TreatmentActionTypes.TOOTH_UPDATED_FAIL: {
      return { ...state, error: action.error };
    }
    case TreatmentActionTypes.ADD_TREATMENT_START: {
      return { ...state, planned: [action.data, ...state.planned] };
    }
    case TreatmentActionTypes.DELETE_TREATMENT_SUCCESS: {
      return {
        ...state,
        planned: state.planned.filter((item) => item.treatmentID !== action.treatmentID),
      };
    }
    case TreatmentActionTypes.TREATMENT_FAIL: {
      return { ...state, error: action.error };
    }
    case TreatmentActionTypes.CHECK_TREATMENT_SUCCESS: {
      return { ...state, teeth: action.data };
    }
    case TreatmentActionTypes.UPDATE_SCHEDULED_TIME: {
      return {
        ...state,
        planned: state.planned.map((item) =>
          item.treatmentID === action.data.treatmentID
            ? { ...item, scheduledTime: action.data.scheduledTime }
            : item,
        ),
      };
    }
    default:
      return state;
  }
};
export default treatmentReducer;
