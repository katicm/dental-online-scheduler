import TreatmentActionTypes from './treatment.types';

export const changeProcedure = (procedure) => ({
  type: TreatmentActionTypes.CHANGE_PROCEDURE,
  procedure,
});
export const changeMode = (mode) => ({
  type: TreatmentActionTypes.CHANGE_MODE,
  mode,
});
export const fetchAllTreatments = () => ({
  type: TreatmentActionTypes.FETCH_ALL_TREATMENTS,
});
export const setAllFetchedTreatments_Success = (data) => ({
  type: TreatmentActionTypes.SET_ALL_FETCHED_TREATMENTS_SUCCESS,
  data,
});
export const setAllFetchedTreatments_Fail = (error) => ({
  type: TreatmentActionTypes.setAllFetchedTreatments_Fail,
  error,
});
export const fetchTreatment = (patientID) => ({
  type: TreatmentActionTypes.START_FETCH_TREATMENT,
  patientID,
});
export const setFetchedTreatments_Success = (data) => ({
  type: TreatmentActionTypes.SET_FETCHED_TREATMENTS_SUCCESS,
  data,
});
export const setFetchedTeeth_Success = (data) => ({
  type: TreatmentActionTypes.SET_FETCHED_TEETH_SUCCESS,
  data,
});
export const setFetchedTreatments_Fail = (error) => ({
  type: TreatmentActionTypes.SET_FETCHED_TREATMENT_FAIL,
  error,
});
export const updateTooth = (data, patientID) => ({
  type: TreatmentActionTypes.UPDATE_TOOTH,
  data,
  patientID,
});
export const toothUpdated = () => ({
  type: TreatmentActionTypes.TOOTH_UPDATED,
});
export const toothUpdated_Fail = (error) => ({
  type: TreatmentActionTypes.SET_FETCHED_TREATMENT_FAIL,
  error,
});
export const addTreatment = (data) => ({
  type: TreatmentActionTypes.ADD_TREATMENT_START,
  data,
});
export const addTreatment_Success = (data) => ({
  type: TreatmentActionTypes.ADD_TREATMENT_SUCCESS,
  data,
});
export const treatment_Fail = (error) => ({
  type: TreatmentActionTypes.TREATMENT_FAIL,
  error,
});
export const deleteTreatment = (treatmentID) => ({
  type: TreatmentActionTypes.DELETE_TREATMENT_START,
  treatmentID,
});
export const deleteTreatment_Success = (treatmentID) => ({
  type: TreatmentActionTypes.DELETE_TREATMENT_SUCCESS,
  treatmentID,
});
export const checkTreatment = (data, patientID) => ({
  type: TreatmentActionTypes.CHECK_TREATMENT,
  data,
  patientID,
});
export const checkTreatment_Success = (data) => ({
  type: TreatmentActionTypes.CHECK_TREATMENT_SUCCESS,
  data,
});
export const updateScheduledTime = (data) => ({
  type: TreatmentActionTypes.UPDATE_SCHEDULED_TIME,
  data,
});
export const updateScheduledTime_Success = () => ({
  type: TreatmentActionTypes.UPDATE_SCHEDULED_TIME_SUCCESS,
});
export const updateScheduledTime_Fail = (error) => ({
  type: TreatmentActionTypes.UPDATE_SCHEDULED_TIME_FAIL,
  error,
});
