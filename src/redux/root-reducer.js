import { combineReducers } from 'redux';
import treatmentReducer from './treatment/treatment.reducer';
import patientReducer from './patient/patient.reducer';
import authReducer from './auth/auth.reducer';

const applicationReducers = combineReducers({
  treatment: treatmentReducer,
  patient: patientReducer,
  auth: authReducer,
});

const rootReducer = (state, action) => {
  return applicationReducers(state, action);
};

export default rootReducer;
