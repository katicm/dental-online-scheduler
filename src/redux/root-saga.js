import { takeEvery } from 'redux-saga/effects';
import TreatmentActionTypes from './treatment/treatment.types';
import PatientActionTypes from './patient/patient.types';
import {
  fetchTreatmentSaga,
  updateToothSaga,
  deleteTreatmentSaga,
  checkTreatmentSaga,
  updateScheduledTimeSaga,
  fetchAllTreatmentsSaga,
} from './treatment/treatment.saga';
import { fetchPatientsSaga } from './patient/patient.saga';

export function* watchTreatment() {
  yield takeEvery(TreatmentActionTypes.START_FETCH_TREATMENT, fetchTreatmentSaga);
  yield takeEvery(TreatmentActionTypes.UPDATE_TOOTH, updateToothSaga);
  //yield takeEvery(TreatmentActionTypes.ADD_TREATMENT_START, addTreatmentSaga);
  yield takeEvery(TreatmentActionTypes.DELETE_TREATMENT_START, deleteTreatmentSaga);
  yield takeEvery(TreatmentActionTypes.CHECK_TREATMENT, checkTreatmentSaga);
  yield takeEvery(TreatmentActionTypes.UPDATE_SCHEDULED_TIME, updateScheduledTimeSaga);
  yield takeEvery(TreatmentActionTypes.FETCH_ALL_TREATMENTS, fetchAllTreatmentsSaga);
}

export function* watchPatient() {
  yield takeEvery(PatientActionTypes.FETCH_PATIENT_START, fetchPatientsSaga);
}
