import React from 'react';

const Footer = () => {
  return (
    <footer className="footer">
      <div className="wrap">
        <span className="copy">© 2020 2PandaDesign</span>
      </div>
    </footer>
  );
};
export default Footer;
