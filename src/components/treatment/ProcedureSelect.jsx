import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import ListSubheader from '@material-ui/core/ListSubheader';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useDispatch } from 'react-redux';
import { changeProcedure } from '../../redux/treatment/treatment.actions';

const ProcedureSelect = () => {
  const dispatch = useDispatch();
  const handleChange = (event) => {
    dispatch(changeProcedure(event.target.value));
  };

  return (
    <FormControl className="procedure-select">
      <InputLabel htmlFor="grouped-select">Procedure</InputLabel>
      <Select onChange={handleChange} defaultValue={'none'} id="grouped-select">
        <MenuItem value="none">
          <span className="legend-color" />
          <em>None</em>
        </MenuItem>
        <ListSubheader>Tooth</ListSubheader>
        <MenuItem value={'missing'}>
          <span style={{ background: 'grey' }} className="legend-color" />
          Missing
        </MenuItem>
        <MenuItem value={'crown'}>
          <span style={{ background: 'darkOrange' }} className="legend-color" />
          Crown
        </MenuItem>
        <MenuItem value={'implant'}>
          <span style={{ background: 'blue' }} className="legend-color" />
          Implant
        </MenuItem>
        <MenuItem value={'root-filling'}>
          <span style={{ background: 'violet' }} className="legend-color" />
          Root Filling
        </MenuItem>
        <MenuItem value={'dentures'}>
          <span style={{ background: 'yellow' }} className="legend-color" />
          Dentures
        </MenuItem>
        <MenuItem value={'bridge'}>
          <span style={{ background: 'darkGreen' }} className="legend-color" />
          Bridge
        </MenuItem>
        <ListSubheader>Crown</ListSubheader>
        <MenuItem value={'missing'}>
          <span style={{ background: 'grey' }} className="legend-color" />
          Missing
        </MenuItem>
        <MenuItem value={'filling'}>
          <span style={{ background: 'red' }} className="legend-color" />
          Filling
        </MenuItem>
      </Select>
    </FormControl>
  );
};
export default ProcedureSelect;
