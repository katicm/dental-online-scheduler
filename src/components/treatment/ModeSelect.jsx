import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { useDispatch } from 'react-redux';
import { changeMode } from '../../redux/treatment/treatment.actions';

const ModeSelect = () => {
  const dispatch = useDispatch();
  const handleChange = (event) => {
    dispatch(changeMode(event.target.value));
  };
  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">Select Mode</FormLabel>
      <RadioGroup onChange={handleChange} row defaultValue="current">
        <FormControlLabel value="none" control={<Radio color="primary" />} label="None" />
        <FormControlLabel value="current" control={<Radio color="primary" />} label="Current" />
        <FormControlLabel value="planning" control={<Radio color="primary" />} label="Planning" />
      </RadioGroup>
    </FormControl>
  );
};
export default ModeSelect;
