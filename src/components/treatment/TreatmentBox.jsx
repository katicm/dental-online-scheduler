import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Jaw from '../teeth/Jaw';
import ProcedureSelect from './ProcedureSelect';
import ModeSelect from './ModeSelect';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useSelector, useDispatch } from 'react-redux';
import { fetchTreatment } from '../../redux/treatment/treatment.actions';
import TreatmentList from './TreatmentList';

const TreatmentBox = () => {
  const { patientID } = useParams();
  const { mode, loading, planned } = useSelector((state) => state.treatment);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchTreatment(patientID));
  }, [dispatch, patientID]);

  return (
    <div className="treatment-box">
      <div className="header-procedure">
        <ModeSelect />
        {mode !== 'none' && <ProcedureSelect />}
      </div>
      <div className="spinner-container">
        {loading ? <CircularProgress size={50} thickness={7} /> : <Jaw />}
      </div>
      <div className="planned-list">
        <TreatmentList planned={planned} />
      </div>
    </div>
  );
};
export default TreatmentBox;
