import React from 'react';
import { ListItem, IconButton, ListItemText } from '@material-ui/core';
import { KeyboardDateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DeleteIcon from '@material-ui/icons/Delete';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { useDispatch } from 'react-redux';
import MomentUtils from '@date-io/moment';
import {
  deleteTreatment,
  checkTreatment,
  updateScheduledTime,
} from '../../redux/treatment/treatment.actions';
import moment from 'moment';

const TreatmentItem = ({ item }) => {
  const dispatch = useDispatch();

  const {
    toothNo,
    treatmentID,
    procedure,
    doctorFullName,
    title,
    zone,
    scheduledTime,
    patientID,
  } = item;

  const handleDelete = () => {
    dispatch(deleteTreatment(treatmentID));
  };

  const handleChecked = () => {
    dispatch(checkTreatment({ ...item, completed: true }, patientID));
  };

  const handleUpdate = (date) => {
    dispatch(updateScheduledTime({ ...item, scheduledTime: date }));
  };

  return (
    <ListItem
      style={{ backgroundColor: scheduledTime > moment().format() ? 'whiteSmoke' : 'tomato' }}
      className="list-item"
    >
      <ListItemText secondary={zone}>
        <div className="tooth-number">{toothNo}</div>
      </ListItemText>
      <ListItemText primary={procedure} />
      <ListItemText primary={'Dr. ' + doctorFullName} secondary={title} />
      <ListItemText>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <KeyboardDateTimePicker
            onChange={handleUpdate}
            value={scheduledTime}
            autoOk
            disableToolbar
            ampm={false}
            disablePast
          />
        </MuiPickersUtilsProvider>
      </ListItemText>
      <IconButton onClick={handleChecked} edge="end">
        <CheckCircleIcon className="treatment-icon" />
      </IconButton>
      <IconButton onClick={handleDelete} edge="end">
        <DeleteIcon className="treatment-icon" />
      </IconButton>
    </ListItem>
  );
};
export default TreatmentItem;
