import React from 'react';
import ProcedureItem from './TreatmentItem';
import { IconButton } from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';

const TreatmentList = ({ planned }) => {
  const handleAdd = () => {
    console.log('Feature is not available :sadface');
  };

  return (
    <div>
      <div className="planned-title">
        <p>Tooth</p>
        <p>Procedure</p>
        <p>Dentist</p>
        <p>Scheduled Time</p>
        <IconButton onClick={handleAdd} className="add-treatment">
          <AddCircleIcon fontSize="large" />
        </IconButton>
      </div>
      {planned.map((item) => (
        <ProcedureItem key={item.treatmentID} item={item} />
      ))}
    </div>
  );
};
export default TreatmentList;
