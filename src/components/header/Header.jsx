import React from 'react';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import BuildIcon from '@material-ui/icons/Build';
import TodayIcon from '@material-ui/icons/Today';
import PaymentIcon from '@material-ui/icons/Payment';
import PeopleIcon from '@material-ui/icons/People';
import HelpIcon from '@material-ui/icons/Help';

const Header = () => {
  const history = useHistory();

  return (
    <div className="header">
      <Button onClick={() => history.push('')} variant="text" className="button">
        <PeopleIcon fontSize="large" />
        Patients
      </Button>
      <Button
        onClick={() => history.push('/scheduler')}
        variant="text"
        color="primary"
        className="button"
      >
        <TodayIcon fontSize="large" />
        Scheduler
      </Button>
      <Button
        onClick={() => history.push('/treatments')}
        variant="text"
        color="primary"
        className="button"
      >
        <BuildIcon fontSize="large" />
        Treatments
      </Button>
      <Button
        onClick={() => history.push('/payments')}
        variant="text"
        color="primary"
        className="button"
      >
        <PaymentIcon fontSize="large" />
        Payments
      </Button>
      <Button
        onClick={() => history.push('/help')}
        variant="text"
        color="primary"
        className="button"
      >
        <HelpIcon fontSize="large" />
        Help
      </Button>
    </div>
  );
};
export default Header;
