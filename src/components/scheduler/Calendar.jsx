import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Paper from '@material-ui/core/Paper';
import LinearProgress from '@material-ui/core/LinearProgress';
import { withStyles } from '@material-ui/core/styles';
import { ViewState, EditingState, IntegratedEditing } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  WeekView,
  DayView,
  Appointments,
  Toolbar,
  DateNavigator,
  ViewSwitcher,
  AppointmentForm,
  AppointmentTooltip,
  TodayButton,
  ConfirmationDialog,
} from '@devexpress/dx-react-scheduler-material-ui';
import moment from 'moment';
import { fetchAllTreatments } from '../../redux/treatment/treatment.actions';

const styles = {
  toolbarRoot: {
    position: 'relative',
    backgroundColor: '#DCDCDC',
  },
  progress: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
    left: 0,
  },
};

const ToolbarWithLoading = withStyles(styles, { name: 'Toolbar' })(
  ({ children, classes, ...restProps }) => (
    <div className={classes.toolbarRoot}>
      <Toolbar.Root {...restProps}>{children}</Toolbar.Root>
      <LinearProgress className={classes.progress} />
    </div>
  ),
);

const mapTreatemntsToAppointments = (treatments) => ({
  ...treatments,
  startDate: moment(treatments.scheduledTime),
  endDate: moment(treatments.scheduledTime).add(treatments.duration, 'minutes'),
  title: treatments.procedure,
  id: treatments.treatmentsID,
  location: treatments.type,
});

const Calendar = () => {
  const data = useSelector((state) => state.treatment.treatments);
  const dispatch = useDispatch();
  const [loading] = useState(false);
  const [current, setCurrent] = useState({
    currentDate: '2020-09-28',
    currentViewName: 'Week',
  });

  const [appointment, setAppointment] = useState({ added: {}, changed: {} });
  const handleViewChange = (viewName) => {
    setCurrent({ ...current, currentViewName: viewName });
  };

  const handleAddedAppointment = (added) => {
    setAppointment({ ...appointment, added });
  };

  const handleChangedAppointment = (changed) => {
    setAppointment({ ...appointment, changed });
  };

  useEffect(() => {
    dispatch(fetchAllTreatments());
  }, [dispatch]);

  const commitChanges = ({ added, changed, deleted }) => {
    /*if (added) setData([...data, { ...added, id: 212121 }]);
    else if (changed) {
      const index = Object.keys(changed)[0];
      var copyData = [...data];
      copyData[index] = { ...data[index], ...changed[index] };
      setData(copyData);
    } else if (deleted) {
      setData(data.filter((appointment) => appointment.id !== deleted));
    }*/
  };

  const { currentDate, currentViewName } = current;
  const mappedData = data ? data.map(mapTreatemntsToAppointments) : [];

  return (
    <Paper>
      <Scheduler data={mappedData} height={860}>
        <ViewState
          currentDate={currentDate}
          currentViewName={currentViewName}
          onCurrentViewNameChange={handleViewChange}
        />
        <EditingState
          onCommitChanges={commitChanges}
          addedAppointment={appointment.added}
          appointmentChanges={appointment.changed}
          onAddedAppointmentChange={handleAddedAppointment}
          onAppointmentChangesChange={handleChangedAppointment}
        />
        <IntegratedEditing />
        <ConfirmationDialog />
        <DayView startDayHour={9} endDayHour={18} />
        <WeekView startDayHour={9} endDayHour={18} />
        <WeekView
          name="work-week"
          displayName="Work Week"
          excludedDays={[0]}
          startDayHour={9}
          endDayHour={18}
        />
        <Appointments />
        <Toolbar {...(loading ? { rootComponent: ToolbarWithLoading } : null)} />
        <DateNavigator />
        <TodayButton />
        <ViewSwitcher />
        <AppointmentTooltip showOpenButton showCloseButton />
        <AppointmentForm />
      </Scheduler>
    </Paper>
  );
};
export default Calendar;
