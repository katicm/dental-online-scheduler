import React from 'react';
import { useDispatch } from 'react-redux';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import { focusPatient } from '../../redux/patient/patient.actions';

const PatientList = ({ patient }) => {
  const dispatch = useDispatch();
  const { patientID, fullPatientName, lastVisit } = patient;

  const handleClick = () => {
    dispatch(focusPatient(patientID));
  };

  return (
    <ListItem key={patientID} button onClick={handleClick}>
      <ListItemAvatar>
        <Avatar>
          <AccountBoxIcon />
        </Avatar>
      </ListItemAvatar>
      <ListItemText primary={fullPatientName} secondary={lastVisit} />
    </ListItem>
  );
};
export default PatientList;
