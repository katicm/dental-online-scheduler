import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import BuildIcon from '@material-ui/icons/Build';
import photo from '../../images/patient.png';
import moment from 'moment';

const PatientFocus = () => {
  const history = useHistory();
  const {
    fullPatientName,
    middleName,
    firstVisit,
    lastVisit,
    allergy,
    notes,
    bloodGroup,
    gender,
    birthDate,
    address,
    email,
    phone,
    patientID,
  } = useSelector((state) => state.patient.focused);

  const handleClick = () => {
    history.push(`/treatment/patient=${patientID}`);
  };

  if (!patientID) return <div />;
  return (
    <div className="patient-focus">
      <div className="patient-basic">
        <Avatar alt="HappySmile" src={photo} className="patient-avatar" />
        <div>
          <span>
            {fullPatientName} ({middleName})
          </span>
          <span>
            {moment().diff(moment(birthDate, 'DD/MM/YYYY'), 'years')} year old {gender} born on{' '}
            {birthDate}
          </span>
        </div>
      </div>
      <div className="patient-info">
        <div className="info-attributes">
          <dl>
            <dt>e-mail</dt>
            <dt>Address</dt>
            <dt>Phone</dt>
            <dt>Blood group</dt>
            <dt>Allergy</dt>
            <dt>First visit</dt>
            <dt>Last visit</dt>
            <dt>Notes</dt>
          </dl>
        </div>
        <Divider orientation="vertical" className="divider" />
        <div className="info-values">
          <dl>
            <dt>{email}</dt>
            <dt>{address}</dt>
            <dt>{phone}</dt>
            <dt>{bloodGroup}</dt>
            <dt>{allergy}</dt>
            <dt>{firstVisit}</dt>
            <dt>{lastVisit}</dt>
            <dt>{notes}</dt>
          </dl>
        </div>
        <Button
          onClick={handleClick}
          className="info-button"
          variant="contained"
          color="primary"
          endIcon={<BuildIcon />}
        >
          Open Treatment Pacient Page
        </Button>
      </div>
    </div>
  );
};
export default PatientFocus;
