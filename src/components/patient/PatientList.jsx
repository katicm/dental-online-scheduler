import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PatientItem from './PatientItem';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import InputBase from '@material-ui/core/InputBase';
import { IconButton } from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';

const PatientList = () => {
  const [search, setSearch] = useState('');
  const patients = useSelector((state) => state.patient.patients);

  const handleChange = (e) => {
    setSearch(e.target.value);
  };

  return (
    <div className="patient-list">
      <List>
        <ListSubheader className="patient-sticky">
          <InputBase
            onChange={handleChange}
            className="patient-sticky-find"
            placeholder="Search…"
          />
          <IconButton>
            <AddCircleIcon fontSize="large" className="patient-sticky-icon" />
          </IconButton>
        </ListSubheader>
        {patients
          .filter((item) => item.fullPatientName.indexOf(search) >= 0)
          .map((item) => (
            <PatientItem key={item.patientID} patient={item} />
          ))}
      </List>
    </div>
  );
};
export default PatientList;
