import React, { useEffect } from 'react';
import { fetchPatients } from '../../redux/patient/patient.actions';
import { useDispatch } from 'react-redux';
import PatientList from './PatientList';
import PatientFocus from './PatientFocus';

const PatientBox = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchPatients());
  }, [dispatch]);

  return (
    <div className="patient-box">
      <PatientList />
      <PatientFocus />
    </div>
  );
};
export default PatientBox;
