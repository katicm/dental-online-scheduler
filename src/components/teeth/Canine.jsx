import React from 'react';
import ToothChart from './ToothChart';
import { toothColor } from '../../utils/teethColor';
import { useTooth } from './useTooth';

const Canine = ({ id }) => {
  const [handleClick, teeth] = useTooth(id);
  const { tooth, chart } = teeth[id];

  const Id = id.slice(1);
  return (
    <div className="combine-tooth">
      <div style={{ fontWeight: 'bold', transform: Id < 30 && 'scaleY(-1)' }}>{Id}</div>
      <ToothChart handleClick={handleClick} chart={chart} />
      <div className="canine">
        <div
          onClick={handleClick}
          id="up"
          title="tooth"
          style={{ background: toothColor(tooth.up) }}
          className="canine-up"
        />
        <div
          onClick={handleClick}
          id="down"
          title="tooth"
          style={{ background: toothColor(tooth.down) }}
          className="canine-down"
        />
      </div>
    </div>
  );
};
export default Canine;
