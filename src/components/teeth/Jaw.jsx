import React from 'react';
import Canine from './Canine';
import Premolar from './Premolar';
import Incisor from './Incisor';
import Molar from './Molar';

const Jaw = () => {
  return (
    <div className="full-jaw">
      <div className="upper-jaw">
        <Molar id={'t18'} />
        <Molar id={'t17'} />
        <Molar id={'t16'} />
        <Premolar id={'t15'} />
        <Premolar id={'t14'} />
        <Canine id={'t13'} />
        <Incisor id={'t12'} />
        <Incisor id={'t11'} />
        <Incisor id={'t21'} />
        <Incisor id={'t22'} />
        <Canine id={'t23'} />
        <Premolar id={'t24'} />
        <Premolar id={'t25'} />
        <Molar id={'t26'} />
        <Molar id={'t27'} />
        <Molar id={'t28'} />
      </div>
      <hr style={{ width: '800px' }} />
      <div className="lower-jaw">
        <Molar id={'t38'} />
        <Molar id={'t37'} />
        <Molar id={'t36'} />
        <Premolar id={'t35'} />
        <Premolar id={'t34'} />
        <Canine id={'t33'} />
        <Incisor id={'t32'} />
        <Incisor id={'t31'} />
        <Incisor id={'t41'} />
        <Incisor id={'t42'} />
        <Canine id={'t43'} />
        <Premolar id={'t44'} />
        <Premolar id={'t45'} />
        <Molar id={'t46'} />
        <Molar id={'t47'} />
        <Molar id={'t48'} />
      </div>
    </div>
  );
};
export default Jaw;
