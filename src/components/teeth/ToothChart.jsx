import React from 'react';
import { chartColor } from '../../utils/teethColor';

const ToothChart = ({ chart: { down, right, left, up, center }, handleClick }) => {
  return (
    <div className="tooth-chart">
      <div onClick={handleClick} id="down" title="chart" style={{ background: chartColor(down) }} />
      <div
        onClick={handleClick}
        id="right"
        title="chart"
        style={{ background: chartColor(right) }}
      />
      <div onClick={handleClick} id="left" title="chart" style={{ background: chartColor(left) }} />
      <div onClick={handleClick} id="up" title="chart" style={{ background: chartColor(up) }} />
      <div
        onClick={handleClick}
        id="center"
        title="chart"
        style={{ background: chartColor(center) }}
      />
    </div>
  );
};
export default ToothChart;
