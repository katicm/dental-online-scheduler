import React from 'react';
import ToothChart from './ToothChart';
import { useTooth } from './useTooth';
import { toothColor } from '../../utils/teethColor';

const Premolar = ({ id }) => {
  const [handleClick, data] = useTooth(id);
  const { tooth, chart } = data[id];

  const Id = id.slice(1);
  return (
    <div className="combine-tooth">
      <div style={{ fontWeight: 'bold', transform: Id < 30 && 'scaleY(-1)' }}>{Id}</div>
      <ToothChart handleClick={handleClick} chart={chart} />
      <div className="premolar">
        <div
          onClick={handleClick}
          id="up"
          title="tooth"
          style={{ background: toothColor(tooth.up) }}
          className="premolar-up"
        />
        <div
          onClick={handleClick}
          id="down"
          title="tooth"
          style={{ background: toothColor(tooth.down) }}
          className="premolar-down"
        />
      </div>
    </div>
  );
};
export default Premolar;
