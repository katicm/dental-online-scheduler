import { useSelector, useDispatch } from 'react-redux';
import { updateTooth, addTreatment } from '../../redux/treatment/treatment.actions';
import { useParams } from 'react-router-dom';
import moment from 'moment';

export const useTooth = (id) => {
  const dispatch = useDispatch();
  const { patientID } = useParams();
  const { procedure, mode, teeth } = useSelector((state) => state.treatment);
  const { doctorFullName, title } = useSelector((state) => state.auth.user);

  const handleClick = (e) => {
    if (mode === 'current')
      dispatch(
        updateTooth(
          { toothID: id, zone: [e.target.id], type: [e.target.title], procedure },
          patientID,
        ),
      );
    else if (mode === 'planning' && procedure !== 'none' && procedure !== 'missing') {
      dispatch(
        addTreatment({
          treatmentID: e.target.title + e.target.id,
          toothNo: id,
          zone: e.target.id,
          type: e.target.title,
          procedure: 'Dental implant',
          doctorFullName: doctorFullName,
          title: title,
          scheduledTime: moment().format(),
          pacientID: patientID,
          //FIXME
        }),
      );
    }
  };
  return [handleClick, teeth];
};
