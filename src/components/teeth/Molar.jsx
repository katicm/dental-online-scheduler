import React from 'react';
import ToothChart from './ToothChart';
import { useTooth } from './useTooth';
import { toothColor } from '../../utils/teethColor';

const Molar = ({ id }) => {
  const [handleClick, data] = useTooth(id);
  const { tooth, chart } = data[id];

  const Id = id.slice(1);
  return (
    <div className="combine-tooth">
      <div style={{ fontWeight: 'bold', transform: Id < 30 && 'scaleY(-1)' }}>{Id}</div>
      <ToothChart handleClick={handleClick} chart={chart} />
      <div className="molar">
        <div
          onClick={handleClick}
          id="down"
          title="tooth"
          style={{ background: toothColor(tooth.down) }}
          className="molar-down"
        />
        <div
          onClick={handleClick}
          id="down_left"
          title="tooth"
          style={{ background: toothColor(tooth.down_left) }}
          className="molar-down molar-down-left"
        />
        <div
          onClick={handleClick}
          id="up"
          title="tooth"
          style={{ background: toothColor(tooth.up) }}
          className="molar-up"
        />
      </div>
    </div>
  );
};
export default Molar;
