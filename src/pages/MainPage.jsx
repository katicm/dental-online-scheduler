import React from 'react';
import PatientBox from '../components/patient/PatientBox';

const MainPage = () => {
  return (
    <div>
      <PatientBox />
    </div>
  );
};
export default MainPage;
