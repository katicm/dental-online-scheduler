import React from 'react';
import TreatmentBox from '../components/treatment/TreatmentBox';

const PatientTreatmentPage = () => {
  return <TreatmentBox />;
};
export default PatientTreatmentPage;
