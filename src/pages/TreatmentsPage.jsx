import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import TreatmentList from '../components/treatment/TreatmentList';
import { fetchAllTreatments } from '../redux/treatment/treatment.actions';

const TreatmentsPage = () => {
  const dispatch = useDispatch();
  const doctorID = useSelector((state) => state.auth.user.doctorID);
  const treatments = useSelector((state) => state.treatment.treatments);

  useEffect(() => {
    dispatch(fetchAllTreatments());
  }, [dispatch]);

  const data = treatments.filter((item) => item.doctorID === doctorID);
  return (
    <div className="treatment-box">
      <div className="planned-list">
        <TreatmentList planned={data} />
      </div>
    </div>
  );
};
export default TreatmentsPage;
