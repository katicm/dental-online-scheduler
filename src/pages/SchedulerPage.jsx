import React from 'react';
import Calendar from '../components/scheduler/Calendar';

const SchedulerPage = () => {
  return <Calendar />;
};
export default SchedulerPage;
